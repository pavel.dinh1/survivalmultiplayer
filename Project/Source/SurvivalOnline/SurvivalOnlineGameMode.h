// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SurvivalOnlineGameMode.generated.h"

UCLASS(minimalapi)
class ASurvivalOnlineGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	TArray<class ASpawnPoint*> SpawnPoints;
	FVector DefaultSpawnLocation;

	UPROPERTY(EditAnywhere, Category = "Respawn")
		float RespawnTime;

public:
	ASurvivalOnlineGameMode();

	virtual void BeginPlay() override;

protected:
	class ASpawnPoint* GetRandomSpawnPoint();

	UFUNCTION()
	void Spawn(AController* Controller);

	virtual void PostLogin(APlayerController* NewPlayer) override;

public:
	void Respawn(AController* Controller);

};



