// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"
#include "Components/StaticMeshComponent.h"
#include "SurvivalOnlineCharacter.h"
#include "PlayerStatsComponent.h"
#include "Net/UnrealNetwork.h"
#include "Engine/Texture2D.h"

// Sets default values
APickup::APickup()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Thumbnail = CreateDefaultSubobject<UTexture2D>(TEXT("Thumbnail"));

	MeshComp->SetupAttachment(GetRootComponent());
	SetReplicates(true);
	SetReplicatingMovement(true);

	IncreaseAmount = 30.f;
	ObjectPickedUp = false;
}

void APickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APickup, ObjectPickedUp);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
}

void APickup::UseItem(ASurvivalOnlineCharacter *Player)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		switch (PickupType)
		{
		case EPickupType::EWater:
			UE_LOG(LogTemp, Warning, TEXT("Adding Thirst"));
			Player->GetPlayerStatsComp()->AddThirst(IncreaseAmount);
			break;
		case EPickupType::EFood:
			UE_LOG(LogTemp, Warning, TEXT("Adding Hunger"));
			Player->GetPlayerStatsComp()->AddHunger(IncreaseAmount);
			break;
		case EPickupType::EHealth:
			UE_LOG(LogTemp, Warning, TEXT("Adding Health"));
			Player->GetPlayerStatsComp()->AddHealth(IncreaseAmount);
			break;
		default:
			break;
		}
		Destroy();
	}
}

void APickup::OnRep_PickedUp()
{
	this->MeshComp->SetHiddenInGame(ObjectPickedUp);
	this->SetActorEnableCollision(!ObjectPickedUp);
}

void APickup::InInventory(bool In)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ObjectPickedUp = In;
		OnRep_PickedUp();
	}
}
