// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

// BASE CHARACTER INCLUDE
#include "SurvivalOnlineCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

// CUSTOM INCLUDE
#include "PlayerStatsComponent.h"
#include "LineTrace.h"
#include "Pickup.h"
#include "SurvivalOnlineGameMode.h"
#include "Inventory.h"

// IN-ENGINE INCLUDE
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Components/SkeletalMeshComponent.h"

//////////////////////////////////////////////////////////////////////////
// ASurvivalOnlineCharacter

ASurvivalOnlineCharacter::ASurvivalOnlineCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation

	PlayerStatsComp = CreateDefaultSubobject<UPlayerStatsComponent>(TEXT("PlayerStatsComponent"));
	LineTraceComp = CreateDefaultSubobject<ULineTrace>(TEXT("LineTraceComponent"));
	bIsSprinting = false;

	Inventory = CreateDefaultSubobject<UInventory>(TEXT("InventoryComponent"));
}

void ASurvivalOnlineCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASurvivalOnlineCharacter::AttemptJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASurvivalOnlineCharacter::StartSprinting);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASurvivalOnlineCharacter::StopSprinting);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ASurvivalOnlineCharacter::Interact);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ASurvivalOnlineCharacter::Attack);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASurvivalOnlineCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASurvivalOnlineCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASurvivalOnlineCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASurvivalOnlineCharacter::LookUpAtRate);

}

void ASurvivalOnlineCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(SprintingHandle, this, &ASurvivalOnlineCharacter::HandleSprinting, 1.f, true);
}

void ASurvivalOnlineCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASurvivalOnlineCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASurvivalOnlineCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		if (!bIsSprinting)
			Value *= 0.5f;

		AddMovementInput(Direction, Value);
	}
}

void ASurvivalOnlineCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction

		if (!bIsSprinting)
			Value *= 0.5f;

		AddMovementInput(Direction, Value);
	}
}

void ASurvivalOnlineCharacter::StartSprinting()
{
	if (PlayerStatsComp->GetStamina() > 10.f)
	{
		bIsSprinting = true;
		if (this->GetVelocity().Size())
			PlayerStatsComp->ControlSprintingTimer(true);
	}
	else if (PlayerStatsComp->GetStamina() < 0.f)
		PlayerStatsComp->ControlSprintingTimer(false);
}

void ASurvivalOnlineCharacter::StopSprinting()
{
	bIsSprinting = false;
	PlayerStatsComp->ControlSprintingTimer(false);
}

void ASurvivalOnlineCharacter::HandleSprinting()
{
	if (bIsSprinting && this->GetVelocity().Size())
	{
		PlayerStatsComp->LowerStamina(2.f);
		if (PlayerStatsComp->GetStamina() <= 0.f)
			StopSprinting();
	}
}

void ASurvivalOnlineCharacter::AttemptJump()
{
	if (PlayerStatsComp->GetStamina() > 10.f && !GetMovementComponent()->IsFalling())
	{
		Jump();
		PlayerStatsComp->LowerStamina(10.f);
	}
}

void ASurvivalOnlineCharacter::Interact()
{
	FVector Start = GetMesh()->GetBoneLocation(FName("head"));
	FVector End = Start + FollowCamera->GetForwardVector() * 170.f;
	FHitResult HitResult = LineTraceComp->LineTraceSingle(Start, End, true);
	if (AActor* Actor = HitResult.GetActor())
	{
		UE_LOG(LogTemp, Warning, TEXT("HIT ACTOR: %s"), *Actor->GetName());
		if (APickup* Pickup = Cast<APickup>(Actor))
		{
			//UE_LOG(LogTemp, Warning, TEXT("Actor is a pickup !"));
			ServerInteract();
		}
	}
}

bool ASurvivalOnlineCharacter::ServerInteract_Validate()
{
	return true;
}

void ASurvivalOnlineCharacter::ServerInteract_Implementation()
{
	if (GetLocalRole() == ROLE_Authority) {
		FVector Start = GetMesh()->GetBoneLocation(FName("head"));
		FVector End = Start + FollowCamera->GetForwardVector() * 170.f;
		FHitResult HitResult = LineTraceComp->LineTraceSingle(Start, End, true);
		if (AActor* Actor = HitResult.GetActor())
		{
			if (APickup* Pickup = Cast<APickup>(Actor))
			{
				Inventory->AddItem(Pickup);
			}
		}
	}
}

void ASurvivalOnlineCharacter::Attack()
{
	FVector Start = GetMesh()->GetBoneLocation(FName("head"));
	FVector End = Start + FollowCamera->GetForwardVector() * 1500.f;
	FHitResult HitResult = LineTraceComp->LineTraceSingle(Start, End, true);
	if (AActor* Actor = HitResult.GetActor())
	{
		if (ASurvivalOnlineCharacter* Player = Cast<ASurvivalOnlineCharacter>(Actor))
		{
			//UE_LOG(LogTemp, Warning, TEXT("HIT ACTOR: %s"),*Actor->GetName());
			ServerAttack();
		}
	}
}

bool ASurvivalOnlineCharacter::ServerAttack_Validate()
{
	return true;
}

void ASurvivalOnlineCharacter::ServerAttack_Implementation()
{
	if (GetLocalRole() == ROLE_Authority) {
		FVector Start = GetMesh()->GetBoneLocation(FName("head"));
		FVector End = Start + FollowCamera->GetForwardVector() * 1500.f;
		FHitResult HitResult = LineTraceComp->LineTraceSingle(Start, End, true);
		if (AActor* Actor = HitResult.GetActor())
		{
			if (ASurvivalOnlineCharacter* Player = Cast<ASurvivalOnlineCharacter>(Actor))
			{
				float TestDamage = 20.f;
				Player->TakeDamage(TestDamage, FDamageEvent(), GetController(), this);
			}
		}
	}
}

void ASurvivalOnlineCharacter::Die()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		Inventory->DropAllItems();
		MultiDie();
		// Start our destroy timer to remove actor from world
		AGameModeBase* GM = GetWorld()->GetAuthGameMode();
		if (ASurvivalOnlineGameMode* GameMode = Cast<ASurvivalOnlineGameMode>(GM))
		{
			GameMode->Respawn(GetController());
		}
		GetWorld()->GetTimerManager().SetTimer(DestroyHandle, this, &ASurvivalOnlineCharacter::CallDestroy, 10.f, false);
	}
}

bool ASurvivalOnlineCharacter::MultiDie_Validate()
{
	return true;
}

void ASurvivalOnlineCharacter::MultiDie_Implementation()
{
	// Ragdoll
	GetCapsuleComponent()->DestroyComponent();
	this->GetCharacterMovement()->DisableMovement();
	this->GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	this->GetMesh()->SetAllBodiesSimulatePhysics(true);
}

void ASurvivalOnlineCharacter::CallDestroy()
{
	if (GetLocalRole() == ROLE_Authority)
		Destroy();
}

float ASurvivalOnlineCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (GetLocalRole() < ROLE_Authority || PlayerStatsComp->GetHealth() <= 0.f)
		return 0.f;

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		PlayerStatsComp->LowerHealth(ActualDamage);
		if (PlayerStatsComp->GetHealth() <= 0.f)
		{
			Die();
		}
	}
	return ActualDamage;
}

FString ASurvivalOnlineCharacter::GetPlayerStats()
{
	return "			Health: " + FString::SanitizeFloat(PlayerStatsComp->GetHealth())
		+ "\n			Hunger: " + FString::SanitizeFloat(PlayerStatsComp->GetHunger())
		+ "\n			Thirst: " + FString::SanitizeFloat(PlayerStatsComp->GetThirst())
		+ "\n			Stamina: " + FString::SanitizeFloat(PlayerStatsComp->GetStamina());
}