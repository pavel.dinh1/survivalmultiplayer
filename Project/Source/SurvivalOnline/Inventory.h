// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Inventory.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SURVIVALONLINE_API UInventory : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInventory();

protected:
	UPROPERTY(Replicated)
	TArray<class APickup *> Items;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_DropItem(class APickup* Item);
	bool Server_DropItem_Validate(class APickup* Item);
	void Server_DropItem_Implementation(class APickup* Item);

	bool CheckIfClientHasItem(class APickup* Item);
	bool RemoveItemFromInventory(class APickup* Item);

public:
	bool AddItem(class APickup *Item);
	void DropAllItems();

	UFUNCTION(BlueprintCallable)
	void DropItem(class APickup *Item);

	UFUNCTION(BlueprintCallable)
	TArray<class APickup *> GetInventoryItems() { return Items; }

	UFUNCTION(BlueprintCallable)
	int32 GetCurrentInventoryCount() { return Items.Num() - 1; }
};
