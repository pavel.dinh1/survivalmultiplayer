// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerStatsComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SURVIVALONLINE_API UPlayerStatsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPlayerStatsComponent();

protected:

	UPROPERTY(Replicated)
		float Hunger;

	UPROPERTY(EditAnywhere, Category = "Survival | PlayerStats")
		float HungerDecrementValue;

	UPROPERTY(Replicated)
		float Thirst;

	UPROPERTY(EditAnywhere, Category = "Survival | PlayerStats")
		float ThirstDecrementValue;

	UPROPERTY(Replicated)
		float Health;

	UPROPERTY(Replicated)
		float Stamina;

	FTimerHandle TimerHandle;

	FTimerHandle StaminaRegeneration;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void HandleHungerAndThirst();

	void RegenerateStamina();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerLowerHunger(float Value);
	bool ServerLowerHunger_Validate(float Value);
	void ServerLowerHunger_Implementation(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerLowerThirst(float Value);
	bool ServerLowerThirst_Validate(float Value);
	void ServerLowerThirst_Implementation(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerLowerStamina(float Value);
	bool ServerLowerStamina_Validate(float Value);
	void ServerLowerStamina_Implementation(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerLowerHealth(float Value);
	bool ServerLowerHealth_Validate(float Value);
	void ServerLowerHealth_Implementation(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerControlSprinting(bool IsSprinting);
	bool ServerControlSprinting_Validate(bool IsSprinting);
	void ServerControlSprinting_Implementation(bool IsSprinting);

public:
	void AddHunger(float Value);
	void AddThirst(float Value);
	void AddHealth(float Value);

	void LowerHunger(float Value);
	void LowerThirst(float Value);
	void LowerStamina(float Value);
	void LowerHealth(float Value);

	float GetHunger();
	float GetThirst();
	float GetStamina();
	float GetHealth();

	void ControlSprintingTimer(bool IsSprinting);
};
