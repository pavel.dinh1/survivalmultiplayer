// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory.h"
#include "Pickup.h"

#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"

// Sets default values for this component's properties
UInventory::UInventory()
{
	SetIsReplicated(true);
}

// Called when the game starts
void UInventory::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UInventory::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UInventory, Items, COND_OwnerOnly);
}

bool UInventory::AddItem(APickup *Item)
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		Items.Add(Item);
		Item->InInventory(true);

		for (APickup *Pickup : Items)
		{
			UE_LOG(LogTemp, Warning, TEXT("Item: %s"), *Pickup->GetName());
		}
		UE_LOG(LogTemp, Warning, TEXT("END OF ITEMS"));
	}
	return false;
}

void UInventory::DropAllItems()
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		for (APickup *Pickup : Items)
		{
			DropItem(Pickup);
		}
		Items.Empty();
	}
}

void UInventory::DropItem(APickup *Item)
{
	Server_DropItem(Item);
}

bool UInventory::Server_DropItem_Validate(APickup* Item)
{
	return CheckIfClientHasItem(Item);
}

void UInventory::Server_DropItem_Implementation(APickup* Item)
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		FVector location = GetOwner()->GetActorLocation();
		location.X += FMath::RandRange(-50.f, 100.f);
		location.Y += FMath::RandRange(-50.f, 100.f);

		FHitResult hitResult;
		FCollisionObjectQueryParams objParams;
		FCollisionQueryParams params;
		params.AddIgnoredActor(GetOwner());

		FVector EndRay = location;
		EndRay.Z -= 500.f;

		GetWorld()->LineTraceSingleByObjectType(OUT hitResult, location, EndRay, objParams, params);

		if (hitResult.ImpactPoint != FVector::ZeroVector)
		{
			location = hitResult.ImpactPoint;
		}

		Item->SetActorLocation(location);
		Item->InInventory(false);

		RemoveItemFromInventory(Item);
	}
}

bool UInventory::CheckIfClientHasItem(APickup* Item)
{
	for (APickup* Pickup : Items)
	{
		if (Pickup == Item)
		{
			return true;
		}
	}
	return false;
}

bool UInventory::RemoveItemFromInventory(APickup* Item)
{
	int32 Counter = 0;
	for (APickup* Pickup : Items)
	{
		if (Pickup == Item)
		{
			Items.RemoveAt(Counter);
			return true;
		}
		++Counter;
	}
	return false;
}
