// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStatsComponent.h"
#include "Net/UnrealNetwork.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "SurvivalOnlineCharacter.h"

// Sets default values for this component's properties
UPlayerStatsComponent::UPlayerStatsComponent()
{
	Hunger = 100.f;
	Thirst = 100.f;
	ThirstDecrementValue = 0.5f;
	HungerDecrementValue = 0.5f;
	Stamina = 100.f;
	Health = 30.f;
}


// Called when the game starts
void UPlayerStatsComponent::BeginPlay()
{
	Super::BeginPlay();
	SetIsReplicated(true);

	if (GetOwnerRole() == ROLE_Authority)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UPlayerStatsComponent::HandleHungerAndThirst, 3.f, true);
		// Regenerate stamina by 1 every second
		GetWorld()->GetTimerManager().SetTimer(StaminaRegeneration, this, &UPlayerStatsComponent::RegenerateStamina, 1.f, true);
	}
}

void UPlayerStatsComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UPlayerStatsComponent, Thirst, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UPlayerStatsComponent, Hunger, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UPlayerStatsComponent, Health, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UPlayerStatsComponent, Stamina, COND_OwnerOnly);
}

void UPlayerStatsComponent::HandleHungerAndThirst()
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		LowerHunger(HungerDecrementValue);
		LowerThirst(ThirstDecrementValue);
	}
}

void UPlayerStatsComponent::LowerHunger(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerLowerHunger(Value);
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		Hunger -= Value;

		if (Hunger < 0)
			if (ASurvivalOnlineCharacter* character = Cast<ASurvivalOnlineCharacter>(GetOwner()))
				character->TakeDamage(Hunger * -1, FDamageEvent(), character->GetController(), character);
	}
}

void UPlayerStatsComponent::LowerThirst(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerLowerThirst(Value);
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		Thirst -= Value;

		if (Thirst < 0)
			if (ASurvivalOnlineCharacter* character = Cast<ASurvivalOnlineCharacter>(GetOwner()))
				character->TakeDamage(Thirst * -1, FDamageEvent(), character->GetController(), character);
	}
}

void UPlayerStatsComponent::LowerStamina(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerLowerStamina(Value);
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		if (Stamina - Value < 0.f)
			Stamina = 0.f;
		else
			Stamina -= Value;
	}
}

void UPlayerStatsComponent::LowerHealth(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerLowerHealth(Value);
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		if (Health - Value < 0.f)
			Health = 0.f;
		else
			Health -= Value;
	}
}

void UPlayerStatsComponent::ControlSprintingTimer(bool IsSprinting)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerControlSprinting(IsSprinting);
		return;
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		if (IsSprinting)
			GetWorld()->GetTimerManager().PauseTimer(StaminaRegeneration);
		else
			GetWorld()->GetTimerManager().UnPauseTimer(StaminaRegeneration);
	}
}

float UPlayerStatsComponent::GetHunger()
{
	return Hunger;
}

float UPlayerStatsComponent::GetThirst()
{
	return Thirst;
}

float UPlayerStatsComponent::GetStamina()
{
	return Stamina;
}

float UPlayerStatsComponent::GetHealth()
{
	return Health;
}


bool UPlayerStatsComponent::ServerLowerHunger_Validate(float Value)
{
	return true;
}

void UPlayerStatsComponent::ServerLowerHunger_Implementation(float Value)
{
	if (GetOwnerRole() == ROLE_Authority)
		LowerHunger(Value);
}

bool UPlayerStatsComponent::ServerLowerThirst_Validate(float Value)
{
	return true;
}

void UPlayerStatsComponent::ServerLowerThirst_Implementation(float Value)
{
	if (GetOwnerRole() == ROLE_Authority)
		LowerThirst(Value);
}

bool UPlayerStatsComponent::ServerLowerStamina_Validate(float Value)
{
	return true;
}

void UPlayerStatsComponent::ServerLowerStamina_Implementation(float Value)
{
	if (GetOwnerRole() == ROLE_Authority)
		LowerStamina(Value);
}

bool UPlayerStatsComponent::ServerLowerHealth_Validate(float Value)
{
	return true;
}

void UPlayerStatsComponent::ServerLowerHealth_Implementation(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
		LowerHealth(Value);
}

bool UPlayerStatsComponent::ServerControlSprinting_Validate(bool IsSprinting)
{
	return true;
}

void UPlayerStatsComponent::ServerControlSprinting_Implementation(bool IsSprinting)
{
	if (GetOwnerRole() == ROLE_Authority)
		ControlSprintingTimer(IsSprinting);
}

void UPlayerStatsComponent::AddHunger(float Value)
{
	if (Hunger + Value >= 100.f)
		Hunger = 100.f;
	else
		Hunger += Value;
}

void UPlayerStatsComponent::AddThirst(float Value)
{
	if (Thirst + Value >= 100.f)
		Thirst = 100.f;
	else
		Thirst += Value;
}

void UPlayerStatsComponent::AddHealth(float Value)
{
	if (Health + Value >= 100.f)
		Health = 100.f;
	else
		Health += Value;
}

void UPlayerStatsComponent::RegenerateStamina()
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		if (Stamina >= 100.f)
			Stamina = 100.f;
		else
			// Amount of regenerated stamina
			++Stamina;
	}
}
