// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UENUM(BlueprintType)
enum class EPickupType : uint8
{
	EWater UMETA(DisplayName = "Water"),
	EFood UMETA(DisplayName = "Food"),
	EHealth UMETA(DisplayName = "Health")
};

UCLASS()
class SURVIVALONLINE_API APickup : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickup();

protected:
	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent *MeshComp;

	UPROPERTY(EditAnywhere)
	class UTexture2D *Thumbnail;

	UPROPERTY(EditAnywhere)
	float IncreaseAmount;

	UPROPERTY(EditAnywhere, Category = "Enums")
	EPickupType PickupType;

	UPROPERTY(ReplicatedUsing = OnRep_PickedUp)
	bool ObjectPickedUp;

	UFUNCTION()
	void OnRep_PickedUp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void UseItem(class ASurvivalOnlineCharacter *Player);
	void InInventory(bool In);

	UFUNCTION(BlueprintCallable)
	class UTexture2D *GetThumbnail() { return Thumbnail; }
};
