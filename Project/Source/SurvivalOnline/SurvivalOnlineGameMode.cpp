// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SurvivalOnlineGameMode.h"
#include "SurvivalOnlineCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Math/UnrealMath.h"
#include "TimerManager.h"

// CUSTOM INCLUDES
#include "SpawnPoint.h"

ASurvivalOnlineGameMode::ASurvivalOnlineGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	DefaultSpawnLocation = FVector(-360.f, 30.f, 150.f);
	RespawnTime = 3.f;
}

void ASurvivalOnlineGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Get all SpawnPoints in the world and store it inside 'SpawnPoints'
	UClass* SpawnPointClass = ASpawnPoint::StaticClass();
	for (TActorIterator<AActor> Actor(GetWorld(), SpawnPointClass); Actor; ++Actor)
	{
		SpawnPoints.Add(Cast<ASpawnPoint>(*Actor));
	}
	UE_LOG(LogTemp, Warning, TEXT("Spawn Points: %d"), SpawnPoints.Num());
}

ASpawnPoint* ASurvivalOnlineGameMode::GetRandomSpawnPoint()
{
	for (int32 i = 0; i < SpawnPoints.Num(); ++i)
	{
		int32 Slot = FMath::RandRange(0, SpawnPoints.Num() - 1);
		if (SpawnPoints[Slot])
			return SpawnPoints[Slot];
	}
	return nullptr;
}

void ASurvivalOnlineGameMode::Spawn(AController* Controller)
{
	if (ASpawnPoint* SpawnPoint = GetRandomSpawnPoint())
	{
		FVector Location = SpawnPoint->GetActorLocation();
		FRotator Rotation = SpawnPoint->GetActorRotation();
		if (APawn* Pawn = GetWorld()->SpawnActor<APawn>(DefaultPawnClass, Location, Rotation))
		{
			Controller->Possess(Pawn);
		}
	}
	else
	{
		FVector Location = DefaultSpawnLocation;
		if (APawn* Pawn = GetWorld()->SpawnActor<APawn>(DefaultPawnClass, Location, FRotator::ZeroRotator))
		{
			Controller->Possess(Pawn);
		}
	}
}

void ASurvivalOnlineGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if (AController* Controller = Cast<AController>(NewPlayer))
		Spawn(Controller);
}

void ASurvivalOnlineGameMode::Respawn(AController* Controller)
{
	if (Controller)
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			FTimerDelegate RespawnDele;
			RespawnDele.BindUFunction(this, FName("Spawn"), Controller);
			FTimerHandle RespawnHandle;
			GetWorld()->GetTimerManager().SetTimer(RespawnHandle, RespawnDele, RespawnTime, false);
		}
	}
}