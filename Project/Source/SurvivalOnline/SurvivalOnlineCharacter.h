// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SurvivalOnlineCharacter.generated.h"

UCLASS(config = Game)
class ASurvivalOnlineCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent *CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent *FollowCamera;

public:
	ASurvivalOnlineCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UPlayerStatsComponent *PlayerStatsComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UInventory *Inventory;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class ULineTrace *LineTraceComp;

	bool bIsSprinting;

	FTimerHandle SprintingHandle;

	UFUNCTION(BlueprintPure)
	FString GetPlayerStats();

protected:
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void StartSprinting();

	void StopSprinting();

	void HandleSprinting();

	void AttemptJump();

	void Interact();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerInteract();
	bool ServerInteract_Validate();
	void ServerInteract_Implementation();

	void Attack();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerAttack();
	bool ServerAttack_Validate();
	void ServerAttack_Implementation();

	void Die();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void MultiDie();
	bool MultiDie_Validate();
	void MultiDie_Implementation();

	FTimerHandle DestroyHandle;

	void CallDestroy();
	/**
		* Called via input to turn at a given rate.
		* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
		*/
	void TurnAtRate(float Rate);

	/**
		* Called via input to turn look up/down at a given rate.
		* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
		*/
	void LookUpAtRate(float Rate);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	class UInventory *GetInventoryComp() { return Inventory; }

public:
	class UPlayerStatsComponent *GetPlayerStatsComp() { return PlayerStatsComp; }

public:
	virtual float TakeDamage(float Damage, struct FDamageEvent const &DamageEvent, class AController *EventInstigator, class AActor *DamageCauser) override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent *GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent *GetFollowCamera() const { return FollowCamera; }
};