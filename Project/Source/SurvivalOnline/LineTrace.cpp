// Fill out your copyright notice in the Description page of Project Settings.


#include "LineTrace.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
ULineTrace::ULineTrace()
{

}


// Called when the game starts
void ULineTrace::BeginPlay()
{
	Super::BeginPlay();

}

FHitResult ULineTrace::LineTraceSingle(FVector Start, FVector End)
{
	FHitResult hitResult;
	FCollisionObjectQueryParams CollisionParams;
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(GetOwner());

	if (GetWorld()->LineTraceSingleByObjectType(OUT hitResult, Start, End, CollisionParams, CollisionQueryParams))
		return hitResult;
	else
		return hitResult;
}

FHitResult ULineTrace::LineTraceSingle(FVector Start, FVector End, bool ShowDebugLine)
{
	FHitResult Actor = LineTraceSingle(Start, End);
	if (ShowDebugLine)
		DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 3.f, 0, 5.f);
	return Actor;
}

